import {
  async,
  fakeAsync,
  ComponentFixture,
  TestBed,
  tick
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { ChartComponent } from './chart.component';
import {
  GoogleChartsModule,
  GoogleChartComponent
} from 'angular-google-charts';
import { Observable, Subscriber } from 'rxjs';

describe('ChartComponent', () => {
  let component: ChartComponent;
  let fixture: ComponentFixture<ChartComponent>;
  let mockData = [];
  let emit: Subscriber<any>;

  beforeEach(async(() => {
    mockData = [['Wed Jan 24 2018', 176.46], ['Thu Jan 25 2018', 175.68]];
    TestBed.configureTestingModule({
      imports: [GoogleChartsModule.forRoot()],
      declarations: [ChartComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartComponent);
    component = fixture.componentInstance;
    component.data$ = new Observable(emitter => (emit = emitter));
    fixture.detectChanges();
  });

  it('should create with no observable set', () => {
    // recreate the component without setting component.data$
    fixture = TestBed.createComponent(ChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should create with some observable', () => {
    expect(component).toBeTruthy();
  });

  it('should create a chart with observable and data is present', fakeAsync(() => {
    // send a real payload of data
    emit.next(mockData);
    fixture.detectChanges();

    // the chart exists as the only child with the name google-chart
    const children = fixture.debugElement.children;
    expect(children.length).toBe(1);
    expect(children[0].name).toBe('google-chart');
  }));

  it('should not create a chart with observable and data is empty', () => {
    // the chart should not exist, because the input data is an empty array
    const children = fixture.debugElement.children;
    expect(children.length).toBe(0);
  });

  it('should not create a chart with observable and data is empty, and still not create when empty data comes back', () => {
    // the chart should not exist, because the input data is an empty array
    const children = fixture.debugElement.children;
    expect(children.length).toBe(0);

    // push an empty array through the observer
    emit.next([]);
    fixture.detectChanges();
    expect(children.length).toBe(0);

    // push null through the observer
    emit.next();
    fixture.detectChanges();
    expect(children.length).toBe(0);
  });
});
