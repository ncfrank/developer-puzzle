import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'coding-challenge-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit, OnDestroy {
  @Input() data$: Observable<any>;

  // using setter to also set values on the chart
  @Input() set title(value: string) {
    this._title = value;
    if (this.chart) {
      this.chart.options.title = this.chart.title = value;
    }
  }
  get title() {
    return this._title;
  }
  private _title: string;

  destroyed$: Subject<boolean> = new Subject();
  chart: {
    title: string;
    type: string;
    data: any;
    columnNames: string[];
    options: any;
  };

  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.chart = {
      title: this.title,
      type: 'LineChart',
      data: [],
      columnNames: ['period', 'close'],
      options: { title: this.title, width: '600', height: '400' }
    };

    // set a default observer in case none is set to prevent failure
    if (!this.data$) {
      this.data$ = new Observable();
    }

    this.data$.pipe(takeUntil(this.destroyed$)).subscribe(newData => {
      if (newData) {
        // format the data to something more humanly readible
        this.chart.data = newData.map(data => [
          new Date(data[0]).toDateString(),
          data[1]
        ]);
      }
    });
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
