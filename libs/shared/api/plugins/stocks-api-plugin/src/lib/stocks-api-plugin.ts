'use strict';
import axios from 'axios';

import Joi from '@hapi/joi';

interface CacheKey {
  segment: string;
  id: string;
}

export interface StocksApiOptions {
  apiKey: string,
  apiURL: string,
  msToRetainInCache: number
}

/**
 * StocksAPIPlugin
 * APIS for retrieving stock information from stock API and
 * leveraging HAPI catbox API to cache results
 */
export const stocksAPIPlugin = {
  name: 'stocksAPIPlugin',
  version: '1.0.0',
  register: async function(server, options) {

    console.log('options', options);
    const stocksApiOptions: StocksApiOptions = options;

    // create the cache for stocks
    const stocksCache = server.cache({
      segment: 'stocks',
      expiresIn: stocksApiOptions.msToRetainInCache
    });

    // handler for cached stocks request
    const getCachedStocksHandler = async (request, h) => {
      const { symbol, range } = request.params;
      const url = `${
        stocksApiOptions.apiURL
      }/beta/stock/${symbol}/chart/${range}?token=${stocksApiOptions.apiKey}`;

      // create a key for lookup in the cache based on inputs
      const key: CacheKey = {
        segment: 'stocks',
        id: `${symbol}:${range}`
      };

      // get item from cache segment
      const cached = await stocksCache.get(key);

      // the item is in the cache, return it
      if (cached && cached.item) {
        return cached.item;
      }

      // item isn't in the cache, fetch it fresh, and set it in the cache
      return axios
        .get(url)
        .then(function(stockResponse) {
          stocksCache.set(
            key,
            { item: stockResponse.data },
            stocksApiOptions.msToRetainInCache
          );
          return stockResponse.data;
        })
        .catch(function(error) {
          return error;
        });
    };

    // add in cached stock route
    server.route({
      method: 'GET',
      path: '/api/stocks/{symbol}/{range}/',
      handler: getCachedStocksHandler,
      config: {
        validate: {
          params: Joi.object({
            symbol: Joi.string()
              .min(1)
              .max(6)
              .required(),
            range: Joi.string()
              .min(1)
              .max(3)
              .valid(
                'max',
                '5y',
                '2y',
                '1y',
                'ytd',
                '6m',
                '3m',
                '1m',
                '1mm',
                '5d',
                '5dm'
              )
              .required()
          })
        }
      }
    });
  }
};
