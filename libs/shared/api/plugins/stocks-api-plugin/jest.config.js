module.exports = {
  name: 'shared-api-plugins-stocks-api-plugin',
  preset: '../../../../../jest.config.js',
  coverageDirectory:
    '../../../../../coverage/libs/shared/api/plugins/stocks-api-plugin',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
