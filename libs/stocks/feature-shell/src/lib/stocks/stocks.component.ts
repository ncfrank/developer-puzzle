import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';
import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl
} from '@angular/forms';

import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';

// set the maximum date in history to go back
const MIN_DATE_MAX_YEAR_DIFFERENCE = 5;
// always use max as period to get one API call that we filter client side based on date
const API_STOCK_FETCH_PERIOD = 'max';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent {
  stockPickerForm: FormGroup;
  dateFromControl: AbstractControl;
  dateToControl: AbstractControl;

  dateFrom: Date;
  dateTo: Date;
  maxDate = new Date();
  minDate = new Date();
  quotes$ = this.priceQuery.priceQueriesInDateRange$;
  symbol: string;
  title: string;

  constructor(
    private fb: FormBuilder,
    private priceQuery: PriceQueryFacade,
    private datePipe: DatePipe
  ) {
    // custom date selection needs to be at most least 5 years ago based on API limit
    this.minDate.setFullYear(
      this.maxDate.getFullYear() - MIN_DATE_MAX_YEAR_DIFFERENCE
    );

    // setup the form
    this.stockPickerForm = fb.group({
      symbol: [null, Validators.required],
      dateTo: [null],
      dateFrom: [null]
    });

    // keep reference to the date fields for validation
    this.dateFromControl = this.stockPickerForm.controls['dateFrom'];
    this.dateToControl = this.stockPickerForm.controls['dateTo'];
  }

  /**
   * dateChanged
   * @description Handler called when the date picker input is changed to updating the subset
   * of data displayed. Tracked as a separate listener than the input so date selection can
   * update selected data without an API call. Also handles the case where a dateFrom is after
   * a dateTo which creates an invalid date range.
   */
  dateChanged() {
    const { dateTo, dateFrom } = this.stockPickerForm.value;
    this.dateTo = dateTo;
    this.dateFrom = dateFrom;

    if (dateFrom && dateTo && dateFrom > dateTo) {
      // set an incorrect date range to be the same value
      this.dateFromControl.setValue(dateTo);
      this.priceQuery.selectDates(dateTo, dateTo);
    } else if (dateFrom && dateTo) {
      // update the data
      this.updateTitle();
      this.priceQuery.selectDates(dateFrom, dateTo);
    }
  }

  /**
   * symbolChanged
   * @description Handler called when the text input is changed to trigger fresh data loading.
   * Created as a separate listener than the Date Pickers so date selection can update selected
   * data without an API call
   */
  symbolChanged() {
    if (this.stockPickerForm.valid) {
      const { symbol } = this.stockPickerForm.value;
      this.symbol = symbol;
      this.updateTitle();
      this.priceQuery.fetchQuote(symbol, API_STOCK_FETCH_PERIOD);
    }
  }

  updateTitle() {
    if (this.dateFrom && this.dateTo) {
      this.title = `${
        this.symbol
      } stock prices between ${this.datePipe.transform(
        this.dateFrom,
        'longDate'
      )} and ${this.datePipe.transform(this.dateTo, 'longDate')}`;
    } else {
      this.title = `${this.symbol} stock prices`;
    }
  }
}
