import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DatePipe } from '@angular/common';

import { StocksComponent } from './stocks.component';
import { SharedUiChartModule } from '@coding-challenge/shared/ui/chart';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { StoreModule } from '@ngrx/store';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Observable, Subscriber } from 'rxjs';

describe('StocksComponent', () => {
  let component: StocksComponent;
  let fixture: ComponentFixture<StocksComponent>;
  let priceFacade: PriceQueryFacade;
  let fetchQuoteSpy;
  let selectDatesSpy;

  let subscriber: Subscriber<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        SharedUiChartModule,
        StoreModule.forRoot({})
      ],
      declarations: [StocksComponent],
      providers: [
        {
          provide: PriceQueryFacade,
          useValue: {
            fetchQuote: function(symbol, period) {},
            selectDates: function(dateFrom, dateTo) {}
          }
        },
        DatePipe
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksComponent);
    priceFacade = fixture.debugElement.injector.get(PriceQueryFacade);
    fetchQuoteSpy = jest.spyOn(priceFacade, 'fetchQuote');
    selectDatesSpy = jest.spyOn(priceFacade, 'selectDates');
    component = fixture.componentInstance;
    component.quotes$ = new Observable(subscribe => (subscriber = subscribe));
    fixture.detectChanges();
  });

  it('should create', () => {
    // make sure basic creation works
    expect(component).toBeTruthy();
  });

  it('should make a request for data when a symbol is entered with no dates', done => {
    // initial values
    const symbolValueToTest = 'tmus';
    const periodValueToTest = 'max';

    // set the value in the form
    const state = {
      symbol: symbolValueToTest,
      dateFrom: null,
      dateTo: null
    };
    component.stockPickerForm.setValue(state);

    // send the event
    const el = fixture.nativeElement.querySelector(
      'input[formControlName="symbol"]'
    );
    el.dispatchEvent(new Event('change'));

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(fetchQuoteSpy).toHaveBeenCalledWith(
        symbolValueToTest,
        periodValueToTest
      );
      expect(selectDatesSpy).not.toHaveBeenCalled();
      done();
    });
  });

  it('should not make a request when a dateFrom is entered', done => {
    // initial values
    const dateFromValueToTest = new Date();

    // set the value in the form
    const state = {
      symbol: null,
      dateFrom: dateFromValueToTest,
      dateTo: null
    };
    component.stockPickerForm.setValue(state);

    // send the event
    const el = fixture.nativeElement.querySelector(
      'input[formControlName="dateFrom"]'
    );
    el.dispatchEvent(new Event('change'));

    // check outcomes
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(fetchQuoteSpy).not.toHaveBeenCalled();
      expect(selectDatesSpy).not.toHaveBeenCalled();
      done();
    });
  });

  it('should not make a request when a dateTo is entered', done => {
    // initial values
    const dateToValueToTest = new Date();

    // set the value in the form
    const state = {
      symbol: null,
      dateFrom: null,
      dateTo: dateToValueToTest
    };
    component.stockPickerForm.setValue(state);

    // send the event
    const el = fixture.nativeElement.querySelector(
      'input[formControlName="dateTo"]'
    );
    el.dispatchEvent(new Event('change'));

    // check outcomes
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(fetchQuoteSpy).not.toHaveBeenCalled();
      expect(selectDatesSpy).not.toHaveBeenCalled();
      done();
    });
  });

  it('should not make a request when a dateFrom and dateTo are entered', done => {
    // initial values
    const dateToValueToTest = new Date();
    const dateFromValueToTest = new Date();

    // set the value in the form
    const state = {
      symbol: null,
      dateFrom: dateFromValueToTest,
      dateTo: dateToValueToTest
    };
    component.stockPickerForm.setValue(state);

    // send the event
    const el = fixture.nativeElement.querySelector(
      'input[formControlName="dateFrom"]'
    );
    el.dispatchEvent(new Event('change'));

    // check outcomes
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(fetchQuoteSpy).not.toHaveBeenCalled();
      expect(selectDatesSpy).toHaveBeenCalled();
      done();
    });
  });

  it('should fix dates when a dateFrom come after a dateTo', done => {
    // initial values
    const dateToValueToTest = new Date();
    const dateFromValueToTest = new Date();
    dateToValueToTest.setDate(dateToValueToTest.getDate() - 1);

    // set the value in the form
    const state = {
      symbol: null,
      dateFrom: dateFromValueToTest,
      dateTo: dateToValueToTest
    };
    component.stockPickerForm.setValue(state);

    // send the event
    const elFrom = fixture.nativeElement.querySelector(
      'input[formControlName="dateFrom"]'
    );
    const elTo = fixture.nativeElement.querySelector(
      'input[formControlName="dateTo"]'
    );
    elFrom.dispatchEvent(new Event('change'));

    // check outcomes
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(elFrom.value).toEqual(elTo.value);
      expect(fetchQuoteSpy).not.toHaveBeenCalled();
      expect(selectDatesSpy).toHaveBeenCalled();
      done();
    });
  });
});
