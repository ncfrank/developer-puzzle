export type StocksAPIAppConfig = {
  production: boolean;
  apiKey: string;
  apiURL: string;
  msToRetainInCache: number;
};
