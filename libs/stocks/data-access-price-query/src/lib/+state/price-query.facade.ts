import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import {
  FetchPriceQuery,
  SelectDates,
  SelectSymbol
} from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import {
  getSelectedSymbol,
  getAllPriceQueries,
  getPriceQueriesInDateRange
} from './price-query.selectors';
import { map, skip } from 'rxjs/operators';

@Injectable()
export class PriceQueryFacade {
  selectedSymbol$ = this.store.pipe(select(getSelectedSymbol));
  priceQueries$ = this.store.pipe(
    select(getAllPriceQueries),
    skip(1),
    map(priceQueries =>
      priceQueries.map(priceQuery => [priceQuery.date, priceQuery.close])
    )
  );

  priceQueriesInDateRange$ = this.store.pipe(
    select(getPriceQueriesInDateRange),
    skip(1),
    map(priceQueries =>
      priceQueries.map(priceQuery => [priceQuery.date, priceQuery.close])
    )
  );

  constructor(private store: Store<PriceQueryPartialState>) {}

  selectSymbol(symbol: string) {
    this.store.dispatch(new SelectSymbol(symbol));
  }

  /**
   * selectDates
   * @description Dispatches an action to the PriceQuery store to select a subset of data
   * @param dateFrom Date, the date from which to create a subset of results from
   * @param dateTo Date, the date up to which the subset of results should be until
   */
  selectDates(dateFrom: Date, dateTo: Date) {
    this.store.dispatch(new SelectDates(dateFrom, dateTo));
  }

  fetchQuote(symbol: string, period: string) {
    this.store.dispatch(new FetchPriceQuery(symbol, period));
  }
}
