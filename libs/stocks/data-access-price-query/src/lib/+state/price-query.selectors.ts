import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  priceQueryAdapter,
  PriceQueryState,
  PRICEQUERY_FEATURE_KEY
} from './price-query.reducer';

const getPriceQueryState = createFeatureSelector<PriceQueryState>(
  PRICEQUERY_FEATURE_KEY
);

export const getSelectedSymbol = createSelector(
  getPriceQueryState,
  (state: PriceQueryState) => state.selectedSymbol
);

const { selectAll } = priceQueryAdapter.getSelectors();

/**
 * getSelectedDateFrom
 * @description NGRX Selector, retieves the selectedDateFrom from the state
 */
export const getSelectedDateFrom = createSelector(
  getPriceQueryState,
  (state: PriceQueryState) => state.selectedDateFrom
);

/**
 * getSelectedDateTo
 * @description NGRX Selector, retrieves the selectedDateTo from the state
 */
export const getSelectedDateTo = createSelector(
  getPriceQueryState,
  (state: PriceQueryState) => state.selectedDateTo
);

export const getAllPriceQueries = createSelector(
  getPriceQueryState,
  selectAll
);

/**
 * getPriceQueriesInDateRange
 * @description NGRX Selector, uses the selected date from and date to to filter
 * stock prices out from the network responses, this allows for
 * date filtering without needing to make additional API followup
 * calls for the same stock.
 */
export const getPriceQueriesInDateRange = createSelector(
  getPriceQueryState,
  getSelectedDateFrom,
  getSelectedDateTo,
  (state: PriceQueryState, dateFrom: Date, dateTo: Date) => {
    const results = selectAll(state);
    const newResults = results.filter(result => {
      if (!dateFrom && !dateTo) {
        // return all items, the normal case
        return true;
      } else {
        // return items that are between the dates
        const resultDate: Date = new Date(result.date);
        return resultDate >= dateFrom && resultDate <= dateTo;
      }
    });
    return newResults;
  }
);
