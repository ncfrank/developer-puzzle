import { StocksAPIAppConfig } from '@coding-challenge/stocks/data-access-app-config';

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment: StocksAPIAppConfig = {
  production: false,
  apiKey: 'Tpk_97f16fd81fb645aabad63dd28e4f8448',
  apiURL: 'https://sandbox.iexapis.com',
  msToRetainInCache: 10000
};
