/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { Server } from '@hapi/hapi';
import CatboxMemory from '@hapi/catbox-memory';

import { environment } from './environments/environment';
import { stocksAPIPlugin, StocksApiOptions } from '@coding-challenge/shared/api/plugins/stocks-api-plugin';

const init = async () => {
  const server = new Server({
    port: 3333,
    host: 'localhost'
  });

  // setup the caching provider, like CatboxMemory or CatboxRedis
  server.cache.provision({
    provider: {
      constructor: CatboxMemory,
      options: {
        partition: 'stocks'
      }
    },
    name: 'catbox-memory-cache'
  });

  // setup the default route
  server.route({
    method: 'GET',
    path: '/',
    handler: (request, h) => {
      return {
        hello: 'world'
      };
    }
  });

  // options for stocks, don't need to pass in all environment object
  const stocksApiPluginOptions: StocksApiOptions = {
    'apiKey': environment.apiKey,
    'apiURL': environment.apiURL,
    'msToRetainInCache': environment.msToRetainInCache
  };
  await server.register({plugin:stocksAPIPlugin, options:stocksApiPluginOptions});

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log('error ', err);
  process.exit(1);
});

init();
